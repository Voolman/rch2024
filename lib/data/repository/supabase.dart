import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:training_final_4/data/models/ModelDestinations.dart';
import 'package:training_final_4/data/models/ModelPackage.dart';
import 'package:training_final_4/data/models/ModelProfile.dart';

var supabase = Supabase.instance.client;

Future<List<ModelProfile>> getUser() async {
  var response = await supabase.from('profiles').select();
  return response.map((e) => ModelProfile.fromJson(e)).toList();
}

Future<List<ModelPackage>> getOrderInformation() async {
  var response = await supabase.from('orders').select();
  return response.map((e) => ModelPackage.fromJson(e)).toList();
}

Future<List<ModelDestinations>> getDestinationInformation() async {
  var response = await supabase.from('destinations_details').select();
  return response.map((e) => ModelDestinations.fromJson(e)).toList();
}