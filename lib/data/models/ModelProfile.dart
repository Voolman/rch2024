class ModelProfile {
  final String id;
  final String idUser;
  final String fullName;
  final String phone;
  final String? avatar;
  final int balance;
  final bool? order;
  final String? createdAt;


  ModelProfile(
      {required this.id, required this.idUser, required this.fullName, required this.phone, required this.avatar, required this.balance, required this.order, required this.createdAt});

  static ModelProfile fromJson(Map<String, dynamic> json) {
    return ModelProfile(
        id: json['id'],
        idUser: json['id_user'],
        fullName: json['fullname'],
        phone: json['phone'],
        avatar: json['avatar'],
        balance: json['balance'],
        order: json['order'],
        createdAt: json['createdAt']
    );
  }

}