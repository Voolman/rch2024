import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:training_final_4/data/repository/supabase.dart';

Future<void> getPackageInformation(Function onResponse, Function(String) onError) async {
  try{
    var response = await getOrderInformation();
    var res = response.where((element) => element.id == 'b00a97af-f0d5-417b-8a2d-c952a5e66d9e').single;
    var response1 = await getDestinationInformation();
    var res1 = response1.where((element) => element.id == 'b00a97af-f0d5-417b-8a2d-c952a5e66d9e').single;
    onResponse(res, res1);
  }on AuthException catch(e){
    onError(e.message);
  }on PostgrestException catch(e){
    onError(e.toString());
  }on Exception catch(e) {
    onError(e.toString());
  }
}