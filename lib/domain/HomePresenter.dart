import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:training_final_4/data/repository/supabase.dart';

import '../data/models/ModelProfile.dart';

Future<void> getUserData(Function onResponse, Function(String) onError) async {
  try{
    var res = await getUser();
    ModelProfile currentUser = res.where((element) => element.idUser == 'c67c6f5e-4b49-44ea-bf5f-ff2d7419e11d').single;
    onResponse(currentUser);
  }on AuthException catch(e){
    onError(e.message);
  }on PostgrestException catch(e){
    onError(e.toString());
  }on Exception catch(e) {
    onError(e.toString());
  }
}