import 'package:flutter/material.dart';

abstract class ColorsApp{
  abstract final Color primary;
  abstract final Color secondary;
  abstract final Color success;
  abstract final Color warning;
  abstract final Color error;
  abstract final Color info;
  abstract final Color text;
  abstract final Color subtext;
  abstract final Color iconTint;
  abstract final Color hint;
  abstract final Color background;
}

class LightColorsApp extends ColorsApp{
  @override
  // TODO: implement error
  Color get error => const Color.fromARGB(255, 237, 58, 58);

  @override
  // TODO: implement iconTint
  Color get iconTint => const Color.fromARGB(255, 20, 20, 20);

  @override
  // TODO: implement info
  Color get info => const Color.fromARGB(255, 47, 128, 237);

  @override
  // TODO: implement primary
  Color get primary => const Color.fromARGB(255, 5, 96, 250);

  @override
  // TODO: implement secondary
  Color get secondary => const Color.fromARGB(255, 236, 128, 0);

  @override
  // TODO: implement subtext
  Color get subtext => const Color.fromARGB(255, 167, 167, 167);

  @override
  // TODO: implement success
  Color get success => const Color.fromARGB(255, 53, 179, 105);

  @override
  // TODO: implement text
  Color get text => const Color.fromARGB(255, 58, 58, 58);

  @override
  // TODO: implement warning
  Color get warning => const Color.fromARGB(255, 235, 188, 46);

  @override
  // TODO: implement whiteColor
  Color get background => const Color.fromARGB(255, 255, 255, 255);

  @override
  // TODO: implement hint
  Color get hint => const Color.fromARGB(255, 207, 207, 207);
}

class DarkColorsApp extends ColorsApp{
  @override
  // TODO: implement error
  Color get error => const Color.fromARGB(0, 0, 00, 0);

  @override
  // TODO: implement hint
  Color get hint => const Color.fromARGB(0, 0, 00, 0);

  @override
  // TODO: implement iconTint
  Color get iconTint => const Color.fromARGB(0, 0, 00, 0);

  @override
  // TODO: implement info
  Color get info => const Color.fromARGB(0, 0, 00, 0);

  @override
  // TODO: implement primary
  Color get primary => const Color.fromARGB(0, 0, 00, 0);

  @override
  // TODO: implement secondary
  Color get secondary => const Color.fromARGB(0, 0, 00, 0);

  @override
  // TODO: implement subtext
  Color get subtext => const Color.fromARGB(0, 0, 00, 0);

  @override
  // TODO: implement success
  Color get success => const Color.fromARGB(0, 0, 00, 0);

  @override
  // TODO: implement text
  Color get text => const Color.fromARGB(0, 0, 00, 0);

  @override
  // TODO: implement warning
  Color get warning => const Color.fromARGB(0, 0, 00, 0);

  @override
  // TODO: implement whiteColor
  Color get background => const Color.fromARGB(0, 0, 00, 0);
}