import 'package:flutter/material.dart';
import 'package:training_final_4/domain/SendPackagePresenter.dart';
import 'package:training_final_4/presentation/theme/colors.dart';

import '../../data/models/ModelDestinations.dart';
import '../../data/models/ModelPackage.dart';


class SendPackage extends StatefulWidget {
  const SendPackage({super.key});

  @override
  State<SendPackage> createState() => _SendPackageState();
}
Widget currentWidget = SizedBox();
class _SendPackageState extends State<SendPackage> {
  @override
  void initState(){
    super.initState();
    getPackageInformation(
        (res, res1){
          setState(() {
            createList(res, res1);
          });
        },
          (p0) => null
    );
  }

  Widget createList(ModelPackage order, ModelDestinations destinations){
    var colors = LightColorsApp();
    currentWidget = Column(
      children: [
        SizedBox(
          height: 108,
          width: double.infinity,
          child: Column(
            children: [
              SizedBox(
                height: 106,
                width: double.infinity,
                child: Column(
                  children: [
                    const SizedBox(height: 73,),
                    Row(
                      children: [
                        const SizedBox(width: 15),
                        GestureDetector(
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                          child: Image.asset('assets/back.png'),
                        ),
                        const SizedBox(width: 99),
                        Text(
                          'Send a package',
                          style: Theme
                              .of(context)
                              .textTheme
                              .titleMedium
                              ?.copyWith(fontSize: 16),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Container(
                height: 2,
                decoration: const BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Color.fromARGB(38, 0, 0, 0),
                        blurRadius: 2,
                        offset: Offset(0, 2),
                      )
                    ]
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(height: 24),
              Text(
                'Package Information',
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                    color: colors.primary
                ),
              ),
              const SizedBox(height: 8,),
              Text(
                'Origin details',
                style: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 12,
                    color: colors.text
                ),
              ),
              const SizedBox(height: 4,),
              Text(
                  '${order.pointAddress} , ${order.pointCountry}',
                  style: Theme
                      .of(context)
                      .textTheme
                      .titleSmall
              ),
              const SizedBox(height: 4,),
              Text(
                  order.pointPhone,
                  style: Theme
                      .of(context)
                      .textTheme
                      .titleSmall
              ),
              const SizedBox(height: 8,),
              Text(
                  'Destination details',
                  style: Theme
                      .of(context)
                      .textTheme
                      .titleSmall
                      ?.copyWith(color: colors.text)
              ),
              const SizedBox(height: 4,),
              Text(
                  '${destinations.destinationAddress} , ${destinations
                      .destinationCountry}',
                  style: Theme
                      .of(context)
                      .textTheme
                      .titleSmall
              ),
              const SizedBox(height: 4,),
              Text(
                  destinations.destinationPhone,
                  style: Theme
                      .of(context)
                      .textTheme
                      .titleSmall
              ),
              const SizedBox(height: 8,),
              Text(
                'Other details',
                style: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 12,
                    color: colors.text
                ),
              ),
              const SizedBox(height: 4,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                      'Package Items',
                      style: Theme
                          .of(context)
                          .textTheme
                          .titleSmall
                  ),
                  Text(
                      order.packageItem,
                      style: Theme
                          .of(context)
                          .textTheme
                          .titleSmall
                          ?.copyWith(color: colors.secondary)
                  ),
                ],
              ),
              const SizedBox(height: 4,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                      'Weight of items',
                      style: Theme
                          .of(context)
                          .textTheme
                          .titleSmall
                  ),
                  Text(
                      order.packageWeight.toString(),
                      style: Theme
                          .of(context)
                          .textTheme
                          .titleSmall
                          ?.copyWith(color: colors.secondary)
                  ),
                ],
              ),
              const SizedBox(height: 4,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                      'Worth of Items',
                      style: Theme
                          .of(context)
                          .textTheme
                          .titleSmall
                  ),
                  Text(
                      order.packageWorth.toString(),
                      style: Theme
                          .of(context)
                          .textTheme
                          .titleSmall
                          ?.copyWith(color: colors.secondary)
                  ),
                ],
              ),
              const SizedBox(height: 4,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                      'Tracking Number',
                      style: Theme
                          .of(context)
                          .textTheme
                          .titleSmall
                  ),
                  Text(
                      'R-${order.id}',
                      style: Theme
                          .of(context)
                          .textTheme
                          .titleSmall
                          ?.copyWith(color: colors.secondary)
                  ),
                ],
              ),
              const SizedBox(height: 37,),
              Divider(height: 1, color: colors.subtext),
              const SizedBox(height: 8,),
              Text(
                'Charges',
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                    color: colors.primary
                ),
              ),
              const SizedBox(height: 10,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                      'Delivery Charges',
                      style: Theme
                          .of(context)
                          .textTheme
                          .titleSmall
                  ),
                  Text(
                      'N2,500.00',
                      style: Theme
                          .of(context)
                          .textTheme
                          .titleSmall
                          ?.copyWith(color: colors.secondary)
                  ),
                ],
              ),
              const SizedBox(height: 8,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                      'Instant delivery',
                      style: Theme
                          .of(context)
                          .textTheme
                          .titleSmall
                  ),
                  Text(
                      'N300.00',
                      style: Theme
                          .of(context)
                          .textTheme
                          .titleSmall
                          ?.copyWith(color: colors.secondary)
                  ),
                ],
              ),
              const SizedBox(height: 8,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                      'Tax and Service Charges',
                      style: Theme
                          .of(context)
                          .textTheme
                          .titleSmall
                  ),
                  Text(
                      'N140.00',
                      style: Theme
                          .of(context)
                          .textTheme
                          .titleSmall
                          ?.copyWith(color: colors.secondary)
                  ),
                ],
              ),
              const SizedBox(height: 9,),
              Divider(height: 1, color: colors.subtext),
              const SizedBox(height: 4,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                      'Package total',
                      style: Theme
                          .of(context)
                          .textTheme
                          .titleSmall
                  ),
                  Text(
                      'N2940.00',
                      style: Theme
                          .of(context)
                          .textTheme
                          .titleSmall
                          ?.copyWith(color: colors.secondary)
                  ),
                ],
              ),
              const SizedBox(height: 46,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    height: 48,
                    width: 158,
                    child: OutlinedButton(
                        onPressed: () {},
                        style: OutlinedButton.styleFrom(
                            side: BorderSide(color: colors.primary),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8)
                            )
                        ),
                        child: Text(
                          'Report',
                          style: TextStyle(
                              color: colors.primary,
                              fontSize: 16,
                              fontWeight: FontWeight.w700
                          ),
                        )
                    ),
                  ),
                  SizedBox(
                    height: 48,
                    width: 158,
                    child: FilledButton(
                        onPressed: () {},
                        style: FilledButton.styleFrom(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(4)
                          ),
                          backgroundColor: colors.primary,
                        ),
                        child: Text(
                          'Successful',
                          style: TextStyle(
                              color: colors.background,
                              fontSize: 16,
                              fontWeight: FontWeight.w700
                          ),
                        )
                    ),
                  )
                ],
              )
            ],
          ),
        )
      ],
    );
    return currentWidget;
  }


  @override
  Widget build(BuildContext context) {
    var colors = LightColorsApp();
    return Scaffold(
        backgroundColor: colors.background,
        resizeToAvoidBottomInset: false,
        body: currentWidget
    );
  }
}