import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:training_final_4/presentation/pages/tabs/HomePage.dart';
import 'package:training_final_4/presentation/pages/tabs/Profile.dart';
import 'package:training_final_4/presentation/pages/tabs/Track.dart';
import 'package:training_final_4/presentation/pages/tabs/Wallet.dart';
import 'package:training_final_4/presentation/theme/colors.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}
int currentIndex = 0;
class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    var colors = LightColorsApp();
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: colors.background,
        currentIndex: currentIndex,
        selectedItemColor: colors.primary,
          unselectedItemColor: colors.subtext,
          selectedLabelStyle: Theme.of(context).textTheme.titleSmall?.copyWith(color: colors.primary),
          unselectedLabelStyle: Theme.of(context).textTheme.titleSmall,
          elevation: 0,
          type: BottomNavigationBarType.fixed,
          items: [
            BottomNavigationBarItem(icon: SvgPicture.asset('assets/house.svg', color: (currentIndex == 0) ? colors.primary : colors.subtext), label: 'Home'),
            BottomNavigationBarItem(icon: SvgPicture.asset('assets/wallet.svg', color: (currentIndex == 1) ? colors.primary : colors.subtext), label: 'Wallet'),
            BottomNavigationBarItem(icon: SvgPicture.asset('assets/car.svg', color: (currentIndex == 2) ? colors.primary : colors.subtext), label: 'Track'),
            BottomNavigationBarItem(icon: SvgPicture.asset('assets/profile.svg', color: (currentIndex == 3) ? colors.primary : colors.subtext), label: 'Profile'),
          ],
        onTap: (newIndex){
          setState(() {
            currentIndex = newIndex;
          });
        },
      ),
      body: [const HomePage(), const Wallet(), const Track(), const Profile()][currentIndex],
    );
  }
}